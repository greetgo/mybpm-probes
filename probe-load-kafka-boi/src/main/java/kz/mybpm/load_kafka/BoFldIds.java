package kz.mybpm.load_kafka;

import org.bson.types.ObjectId;

@SuppressWarnings("SpellCheckingInspection")
public class BoFldIds {
  public static final ObjectId BO_CLIENT_ID     = new ObjectId("5cc815afa21b95fa247fafc8");
  public static final String   BO_CLIENT_IIN_ID = "~WHJ9Vg0ZZIEcK61";
  public static final String   BO_CLIENT_IMA_ID = "WlrLHI9RO~Ic2fo3";
  public static final String   BO_CLIENT_DR_ID = "x1n0L~EhQplSGDyR";
  public static final String   BO_CLIENT_ST_ID = "fVJhlKmFc6eIJdyS";
}
