package kz.mybpm.load_kafka.util;

import java.util.Map;

public class ConsumerConfig {
  public static void setOptions(Map<String, String> configMap) {
    configMap.put("  auto.commit.interval.ms    ", "       1000    ");
    configMap.put("  fetch.min.bytes            ", "          1    ");
    configMap.put("  max.partition.fetch.bytes  ", "  141943040    ");
    configMap.put("  connections.max.idle.ms    ", "     540000    ");
    configMap.put("  default.api.timeout.ms     ", "      60000    ");
    configMap.put("  fetch.max.bytes            ", "   52428800    ");
    configMap.put("  retry.backoff.ms           ", "       2000    ");
    configMap.put("  session.timeout.ms         ", "      45000    ");
    configMap.put("  heartbeat.interval.ms      ", "       5000    ");
    configMap.put("  max.poll.interval.ms       ", "    3000000    ");
    configMap.put("  max.poll.records           ", "        500    ");
    configMap.put("  receive.buffer.bytes       ", "      65536    ");
    configMap.put("  request.timeout.ms         ", "      30000    ");
    configMap.put("  send.buffer.bytes          ", "     131072    ");
    configMap.put("  fetch.max.wait.ms          ", "        500    ");
  }
}
