package kz.mybpm.load_kafka.util;

import java.nio.charset.StandardCharsets;
import org.apache.kafka.common.serialization.Deserializer;

public class StrDeserializer implements Deserializer<String> {
  @Override
  public String deserialize(String topic, byte[] data) {
    return data == null ? null : new String(data, StandardCharsets.UTF_8);
  }
}
