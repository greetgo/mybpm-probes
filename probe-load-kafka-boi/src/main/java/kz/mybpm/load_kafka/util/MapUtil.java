package kz.mybpm.load_kafka.util;

import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class MapUtil {
  public static Map<String, Object> trimMap(Map<String, String> map) {
    return map.entrySet()
              .stream()
              .map(e -> Map.entry(e.getKey().trim(), e.getValue().trim()))
              .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
