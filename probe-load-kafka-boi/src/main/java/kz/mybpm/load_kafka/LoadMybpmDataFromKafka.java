package kz.mybpm.load_kafka;

import java.io.File;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.greetgo.kafka.model.Box;
import kz.greetgo.mybpm.model_kafka_mongo.kafka_gen.bo.KafkaBoi;
import kz.greetgo.mybpm.model_kafka_mongo.kafka_gen.bo.updates_boi.ChangeBoi;
import kz.greetgo.mybpm.model_kafka_mongo.kafka_gen.bo.updates_boi.ChangeBoi_actual;
import kz.greetgo.mybpm.model_kafka_mongo.kafka_gen.bo.updates_boi.ChangeBoi_archive;
import kz.greetgo.mybpm.model_kafka_mongo.kafka_gen.bo.updates_boi.ChangeBoi_fieldValues;
import kz.greetgo.mybpm.model_kafka_mongo.str_converter.MybpmStrConverterCreator;
import kz.greetgo.mybpm.model_web.web.dynamic_forms.form_field_type.value.Value_DATE;
import kz.greetgo.mybpm.model_web.web.dynamic_forms.form_field_type.value.Value_DROPDOWN_SINGLE;
import kz.greetgo.mybpm.model_web.web.dynamic_forms.form_field_type.value.Value_INPUT_TEXT;
import kz.greetgo.strconverter.StrConverter;
import kz.mybpm.load_kafka.util.ConsumerConfig;
import kz.mybpm.load_kafka.util.MapUtil;
import kz.mybpm.load_kafka.util.StrDeserializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;

import static kz.mybpm.load_kafka.BoFldIds.BO_CLIENT_DR_ID;
import static kz.mybpm.load_kafka.BoFldIds.BO_CLIENT_ID;
import static kz.mybpm.load_kafka.BoFldIds.BO_CLIENT_IIN_ID;
import static kz.mybpm.load_kafka.BoFldIds.BO_CLIENT_IMA_ID;
import static kz.mybpm.load_kafka.BoFldIds.BO_CLIENT_ST_ID;

public class LoadMybpmDataFromKafka {

  public static void main(String[] args) throws Exception {

    File workFile = Paths.get("build").resolve("reading_kafka_working.txt").toFile();
    workFile.getParentFile().mkdirs();
    workFile.createNewFile();

    System.out.println("Wws1MitJ3A :: Чтобы остановить программу удалите файл: " + workFile);

    // В кафке данные хранятся в особом формате в строках в кодировке UTF8.
    // Для преобразования этих строк в Java-объекты необходим этот конвертор.
    StrConverter strConverter = MybpmStrConverterCreator.create();

    Map<String, String> configMap = new HashMap<>();

    // Здесь настраиваются различные временные и прочие параметры.
    ConsumerConfig.setOptions(configMap);


    configMap.put("     auto.offset.reset  ", "  earliest       ");// этот параметр говорит, что нужно считывать сначала
    configMap.put("              group.id  ", "  my_cursor_002  ");// это "курсор" который следить за тем, что прочитано, а что нет
    configMap.put("    enable.auto.commit  ", "  false          ");// false - обозначает, что нужно фиксировать чтение вызывая специальный метод

    configMap.put("bootstrap.servers", "localhost:10011");// Это строка доступа к кафке


    ByteArrayDeserializer forKey   = new ByteArrayDeserializer();// в ключах ничего нет - поэтому их анализировать не нужно
    StrDeserializer       forValue = new StrDeserializer();// здесь сидят строки в кодировке UTF-8

    try (KafkaConsumer<byte[], String> consumer = new KafkaConsumer<>(MapUtil.trimMap(configMap), forKey, forValue)) {

      // список топиков нужно уточнять - у них могут быть префиксы. Но обычно этот список именно такой
      List<String> topics = new ArrayList<>();
      topics.add("BOI");
      topics.add("BOI_PROCESS");
      topics.add("BOI_RETENTION_STANDARD");

      consumer.subscribe(topics);

      System.out.println("Mh74Ml6b8C :: Программа запущена и ожидает появления новых данных");

      while (workFile.exists()) {
        ConsumerRecords<byte[], String> records = consumer.poll(Duration.ofMillis(800));

        boolean wasData = false;

        for (final ConsumerRecord<byte[], String> record : records) {

          String objectSerializedToStr = record.value();
          wasData = true;

          //System.out.println("62t4iau9jw :: serializedObject = " + objectSerializedToStr);

          // все данные обёрнуты в специальный объект
          Box box = strConverter.fromStr(objectSerializedToStr);

          performFromKafka(box);
        }

        if (wasData) {
          // если enable.auto.commit=false, то нужно вызывать этот метод после обработки данных, чтобы повторно не считывать данные из кафки
          consumer.commitSync();
        }
      }
    }

    System.out.println("J7Qi1r8x3q :: выход из программы");
  }


  private static void performFromKafka(Box box) {

    Object body = box.body;

    if (body instanceof KafkaBoi boi) {

      List<ChangeBoi> changes = boi.changes;

      for (final ChangeBoi change : changes) {
        if (change instanceof ChangeBoi_fieldValues fv) {

          String fieldId     = fv.fieldId;
          String storedValue = fv.storedValue;

          if (BO_CLIENT_ID.equals(boi.boId) && BO_CLIENT_IIN_ID.equals(fieldId)) {

            Value_INPUT_TEXT value = Value_INPUT_TEXT.parse(storedValue);

            System.out.println("a8luGmYT4e :: У клиента " + boi.id + " изменился ИИН на " + value.text);
            continue;
          }
          if (BO_CLIENT_ID.equals(boi.boId) && BO_CLIENT_IMA_ID.equals(fieldId)) {

            Value_INPUT_TEXT value = Value_INPUT_TEXT.parse(storedValue);

            System.out.println("a8luGmYT4e :: У клиента " + boi.id + " изменился Имя на " + value.text);
            continue;
          }
          if (BO_CLIENT_ID.equals(boi.boId) && BO_CLIENT_DR_ID.equals(fieldId)) {

            Value_DATE value = Value_DATE.parse(storedValue);

            System.out.println("a8luGmYT4e :: У клиента " + boi.id + " изменился ДР на " + value.date);
            continue;
          }
          if (BO_CLIENT_ID.equals(boi.boId) && BO_CLIENT_ST_ID.equals(fieldId)) {

            Value_DROPDOWN_SINGLE value = Value_DROPDOWN_SINGLE.parse(storedValue);

            String selectedOptionId = value.selectedOptionId;

            if (selectedOptionId == null) {
              System.out.println("ti9jq1f4P0 :: У клиента " + boi.id + " стёрся Статус");
              continue;
            }

            String label = switch (selectedOptionId) {
              case "69e8AnUYeHCy9Wkd" -> "Идёт";
              case "SDra~rvxtTM8KMAb" -> "Сидит";
              case "9@KQwG0rEV9IBW@4" -> "Стоит";
              case "m9UPlJ1f4@VC@nOi" -> "Висит";
              default -> "<Не определён>";
            };

            System.out.println("a8luGmYT4e :: У клиента " + boi.id + " изменился Статус на " + label);

            continue;
          }

          continue;
        }
        if (change instanceof ChangeBoi_actual cba) {
          boolean actual = cba.actual;

          if (actual == true) {
            System.out.println("skH1FL96bu :: Запись клиента " + boi.id + " восстановили");
          } else {
            System.out.println("24bu0sV1RD :: Запись клиента " + boi.id + " удалили");
          }

          continue;
        }
        if (change instanceof ChangeBoi_archive cba) {
          boolean archive = cba.archive;

          if (archive == true) {
            System.out.println("WpEoWQXiIJ :: Запись клиента " + boi.id + " убрали в архив");
          } else {
            System.out.println("008u4VzB3h :: Запись клиента " + boi.id + " восстановили из архива");
          }

          continue;
        }
      }

    }

  }

}
